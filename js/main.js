// mobile nav open and close

$(document).ready(function() {
  // Open and close main nav
  $('.menu-btn').click(function() {
    $('.nav-bottom').toggleClass('open');
    $('.menu-btn').toggleClass('close');
  })

  // Open and close sidebar menu
  $('.btn-round.blue').click(function() {
    $('.content-top-bar').toggleClass('sidebar-open');
    $('.sidebar').toggleClass('open');
  })

  // // Add active class to active link in navigation
  // // --------------------------------------------------
  //
  // // get current path and find target link
  // var path = window.location.pathname.split("").pop();
  //
  // // Account for homepage with empty path
  // if (path == '') {
  //   path == 'index.html';    // replace with home page if this isn't correct
  // }
  //
  // var target = $('.nav-links li a[href="'  '"]');
  //
  // console.log(target);
  //
  // // Add active class to active link
  // target.addClass('active');
})
