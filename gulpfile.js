var gulp        = require('gulp'),
    browserSync = require('browser-sync'),
    sass        = require('gulp-sass'),
    prefix      = require('gulp-autoprefixer')

// browserSync task serves and hot reloads
gulp.task('serve', function() {
  browserSync.init({
    server: {
      baseDir: "./"
    },
    port: 8000,  // Change if necessary
  });
})

// Sass compile task
gulp.task('sass', function () {
  return gulp
  .src('scss/*.scss')
  .pipe(sass().on('error', sass.logError))
  .pipe(prefix({
    browsers: ['last 2 versions'],
    cascade: false,
    remove: true
  }))
  .pipe(gulp.dest('css'))
  .pipe(browserSync.reload({
    stream: true
  }));
})

// Defautl run command "gulp"
gulp.task('default', ['serve', 'sass'], function() {
    gulp.watch('scss/**/*.scss', ['sass']);
    gulp.watch('*.html').on('change', browserSync.reload);
})
